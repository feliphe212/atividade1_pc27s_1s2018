
package udpsockets;

/**
 * Exemplo ilustrativo de comunicacao em rede com Sockets Java 
 * 
 * Autor: Felipe Brena Ribeiro
 * Baseado em: https://systembash.com/ 
 * Ultima modificacao: 14/03/2018 21:54
 *
 */
import java.io.*;
import java.net.*;
import java.util.Random;

public class UDPClient {
    
    public UDPClient()  throws Exception{
        super();
        
        try{
            BufferedReader inFromUser
                = new BufferedReader(new InputStreamReader(System.in));
        DatagramSocket clientSocket = new DatagramSocket();
        InetAddress IPAddress = InetAddress.getByName("localhost");
        byte[] sendData = new byte[1024];
        byte[] receiveData = new byte[1024];
        String sentence = inFromUser.readLine();
        sendData = sentence.getBytes();
        DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length, IPAddress, 9876);
        //Dados do cliente estao inteiramente contidos no pacote
        //Servidor usarah os dados do pacote para responder no socket do cliente
        clientSocket.send(sendPacket);
            String resposta = setTimeout();
            if (resposta.equals("2")){
                System.out.println("TimeOut");
            }else {
                DatagramPacket receivePacket = new DatagramPacket(receiveData, receiveData.length);
                clientSocket.receive(receivePacket); //note que a recepção eh feita no socket cliente
                String modifiedSentence = new String(receivePacket.getData());
                System.out.println("FROM SERVER:" + modifiedSentence);
            }
        clientSocket.close();
        } catch (Exception e){
            
        }
    }

    public static void main(String args[]) throws Exception {
       UDPClient minhaApp = new UDPClient();
    }
    public String setTimeout(){
        String resposta = "";
        try{
            Thread timeout = new Thread ();
            Random tempo = new Random();
            int x = tempo.nextInt();
            resposta = x + "";
            timeout.wait(x);
        }catch (Exception e){          
        }
        return resposta;
    }
}
